# project-ciencia-de-datos-Christian_solis

Project: Comparative data analysis of the molecular dynamics simulations of complexes between matriptase protein and serine protease inhibitors: HAI-1 and HAI-2

Data Science project as a part of Advanced Physiscs Course 2021 (La CongaPhysics)

Description of Project: https://youtu.be/HH4HGmdyti4
